import {Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormGroup, FormControl, Validators} from '@angular/forms';

export interface ExchangeRates {
  ccy: string;
  base_ccy: string;
  buy: number;
  sale: number;
}
export interface Froms {
 value: number;
 viewValue: any;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  privat: ExchangeRates[] = [];
  form: FormGroup;
  From: Froms[] = [
    {value: 1, viewValue: 'UAH to USD'},
    {value: 2, viewValue: 'USD to UAH'}
  ];
  constructor(private http: HttpClient) { }
  ngOnInit() {
    this.form = new FormGroup( {
      input: new FormControl('', [
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(1)
      ]),
      select: new FormControl('', [
        Validators.required
      ]),
    });
    this.http.get<ExchangeRates[]>('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5')
      .subscribe(curs => {this.privat = curs; console.log('curs:', this.privat[0].sale); });
    console.log('form:', this.form);
  }
  showInp() {
    // console.log('select:', this.select.value.)
    const inp = this.form.value.input;
    const curs = this.privat[0].sale;
    // return this.form.value.select;
    if (this.form.value.select === 1) {
      return  (inp / curs).toFixed(2);
    } else if (this.form.value.select === 2) {
      return (inp * curs).toFixed(2);
    }
  }
}

